## alexa-skill-php-template  

### Prerequisites
- PHP >= 7.2
- Composer
- External IP/Domain name
- SSL Certificate

### How-To
1. Clone the repository to a place on your server :
```bash
cd /var/www/
git clone https://gitlab.com/Natizyskunk/alexa-skill-php-template.git
```
2. Install Composer depedencies :
```bash
cd /var/www/alexa-skill-php-template/
composer install
```
3. Go to the alexa developer console https://developer.amazon.com/alexa/console/ask then select your actual skill and go to the endpoint section. <img src="https://i.imgur.com/IN73Ql3.png" alt="preview" height="500" />
4. In the default region add the url pointing to the index.php file of the alexa skill on your server (the one inside the /var/www/alexa-skill-php-template/ folder). Also don't forget to use HTTPS. <img src="https://i.imgur.com/R9cZj4z.png" alt="preview2" height="500" />
5. In the dropdown just under the default region URL select : "My development endpoint is a sub-domain of a domain that has a wildcard certificate from a certificate authority".
6. Then just save your Endpoints and Voilà you're done.
7. Now you can develop your own skill by adding new Intents.
