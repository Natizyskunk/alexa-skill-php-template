<?php
namespace AlexaBundle\Service;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

date_default_timezone_set('Europe/Paris');

class AlexaSkills {
  private $_appId;

  public function __construct() {
    // Replace this with your own skill app ID. You can find it in the url of your skill.
	$this->_appId = 'amzn1.ask.skill.xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx';
  }


  public function getOwnerName() {
	$text = "Hello, the owner of this echo device is <PUT_YOUR_NAME_HERE>.";
	return $text;
  }
  
  public function greeting($name=null) {
	$greetings_options = [
		"Yo",
		"Hello",
		"Good Morning"
	];

	$text = $greetings_options[array_rand($greetings_options)]." ".$name.".";
    return $text;
  }

  public function skillAction($intent, $intentData=null, $sessionId=null) {
	
	switch ($intent) {
	  case 'greeting':
	  	$name = $intentData->name->value ?? '';
		$text = $this->greeting($name);
		$text = html_entity_decode(htmlentities($text, ENT_QUOTES | ENT_SUBSTITUTE, "ISO-8859-15"), ENT_QUOTES);
		break;
	  case 'ownerName':
	  	$text = $this->getOwnerName();
		$text = html_entity_decode(htmlentities($text, ENT_QUOTES | ENT_SUBSTITUTE, "ISO-8859-15"), ENT_QUOTES);
		break;
	  default:
		throw "Invalid intent";
		break;
	}

	$response = [
	  'outputSpeech' => [
		'type' => 'PlainText',
		'text' => $text,
		'ssml' => null
	  ]
	];

    $data = [
      'version' => "0.1",
      'sessionAttributes' => [
        'countActionList' => [
          'read' => true,
          'category' => true
		]
	  ],
      'response' => $response,
      'shouldEndSession' => false
	];

	header ('Content-Type: application/json');
	return json_encode($data);
  }


  // -------------------------


  /**
   * Check if the request is from Amazon Ip
   */
  public function isRequestFromAmazon() {
    $amazon_ip = ["72.21.217.","54.240.197."];
    foreach($amazon_ip as $ip) {
      if (stristr($_SERVER['REMOTE_ADDR'], $ip)) {
        return true;
      }
    }
    return false;
  }

  public function validate($requete) {
    //check if the request come from amazon
    $isAmazonIp = $this->isRequestFromAmazon();
    if (!$isAmazonIp) {
      throw new BadRequestHttpException("Forbidden, your Host is not allowed to make this request!", null, 400);
      return false;
    }

    //check my Amazon IP
    if (strtolower($requete->session->application->applicationId) != strtolower($this->_appId)) {
      throw new BadRequestHttpException("Forbidden, your App ID is not allowed to make this request!", null, 400);
      return false;
    }

    // Check SSL signature
    if (preg_match("/https:\/\/s3.amazonaws.com(\:443)?\/echo.api\/*/i", $_SERVER['HTTP_SIGNATURECERTCHAINURL']) == false) {
      throw new BadRequestHttpException( "Forbidden, unkown SSL Chain Origin!", null, 400);
      return false;
    }

    $pem_file = sys_get_temp_dir() . '/' . hash("sha256", $_SERVER['HTTP_SIGNATURECERTCHAINURL']) . ".pem";
    if (!file_exists($pem_file)) {
      file_put_contents($pem_file, file_get_contents($_SERVER['HTTP_SIGNATURECERTCHAINURL']));
    }
    $pem = file_get_contents($pem_file);

    $json = file_get_contents('php://input');
    if (openssl_verify($json, base64_decode($_SERVER['HTTP_SIGNATURE']) , $pem) !== 1) {
      throw new BadRequestHttpException( "Forbidden, failed to verify SSL Signature!", null, 400);
      return false;
    }

    // check we can parse the pem content
    $cert = openssl_x509_parse($pem);
    if (empty($cert)) {
      throw new BadRequestHttpException("Certificate parsing failed!", null, 400);
      return false;
    }

    // Check subjectAltName
    if (stristr($cert['extensions']['subjectAltName'], 'echo-api.amazon.com') != true) {
      throw new BadRequestHttpException( "Forbidden! Certificate subjectAltName Check failed!", null, 400);
      return false;
    }

    // check expiration date of the certificate
    if ($cert['validTo_time_t'] < time()) {
      throw new BadRequestHttpException( "Forbidden! Certificate no longer Valid!", null, 400);
      if (file_exists($pem_file)) {
        unlink($pem_file);
      }
      return false;
    }

    if (time() - strtotime($requete->request->timestamp) > 60) {
      throw new BadRequestHttpException( "Request Timeout! Request timestamp is to old.",null, 400);
      return false;
    }

    return true;
  }
}

$json    	= file_get_contents('php://input');
$requete 	= json_decode($json);
$intent     = $requete->request->intent->name ?? 'default';
$intentData = $requete->request->intent->slots ?? 'default';
$sessionId 	= $requete->session->sessionId ?? 'default';

if (empty($requete) || !isset($requete)) {
	die('Bad Request.');
}

$alexaSkillsService = new AlexaSkills();
$validate = $alexaSkillsService->validate($requete);

if ($validate === true) {
  echo $alexaSkillsService->skillAction($intent, $intentData);
  die();
}
